require 'digest/md5'
include Nanoc::Helpers::LinkTo

def asset_path(path)
  target = @items[path]
  if !target
    raise RuntimeError("asset not found: #{path}")
  end
  hash = Digest::MD5.hexdigest(File.read(target[:filename]))
  relative_path_to(target) + '?' + hash
end
