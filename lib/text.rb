system('rake', 'mo') or fail

require 'gettext'
GetText.bindtextdomain "minidebconf", path: 'locale'

def language
  item.path =~ %r{^/en} && :en || :'pt-br'
end

def metadata(key)
  @config[:metadata][language][key]
end

def l_(link, target_language=nil)
  target_language ||= language
  if target_language == :en
    '/en' + link
  else
    link
  end
end

def _(text)
  GetText.set_locale language
  GetText._(text)
end
alias :t :_

def change_language
  if language == :en
    link_to('<span class="fa fa-arrow-circle-down"></span> Português', item.path.sub(%r{^/en},''))
  else
    link_to('<span class="fa fa-globe"></span> English', '/en' + item.path)
  end
end
