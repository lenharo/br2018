require 'cgi'
require 'rdiscount'
include Nanoc::Helpers::Rendering

class Activity < Struct.new(:title, :name, :bio, :description)
  def initialize(data)
    self.class.members.each do |field|
      value = data[field.to_s]
      self.send("#{field}=", value)
    end
  end

  def to_s
    d = CGI.escapeHTML(RDiscount.new(description.to_s).to_html)
    t = CGI.escapeHTML(title.to_s)
    b = CGI.escapeHTML(RDiscount.new(bio.to_s).to_html)
    s = CGI.escapeHTML(Array(name).join(', '))
    [
      '<p><strong><a class="title" data-container="body" data-toggle="popover" data-html="true" data-placement="top" title="%s" data-content="%s">%s</a></strong></p>' % [t, d, t],
      '<p><a class="speaker" data-container="body" data-toggle="popover" data-html="true" data-placement="top" title="%s" data-content="%s">%s</a></p>' % [s, b, s],
    ].join("\n\n")
  end
end

def schedule_data
  @schedule_data ||=
    begin
      all = {}
      YAML.load_file('content/schedule-data.yml')[:talks].each do |key,data|
        all[key.to_sym] = Activity.new(data)
      end
      all
    end
end

def schedule(key)
  schedule_data.fetch(key)
end

alias :s :schedule
