---
title: Patrocinadores e Apoiadores
---

# Patrocinadores Ouro

  [link-collabora]: https://www.collabora.com.br
  [image-collabora]: /images/logo-collabora.png "Collabora"

[![Collabora][image-collabora]][link-collabora]

## Patrocinadores Prata

  [link-4linux]: http://www.4linux.com.br
  [image-4linux]: /images/logo-4linux.png "4linux"

[![4linux][image-4linux]][link-4linux]

# Apoiadores

  [link-apufpr]: http://apufpr.org.br
  [image-apufpr]: /images/logo-apufpr.png "Apufpr"

[![Apufpr][image-apufpr]][link-apufpr]



