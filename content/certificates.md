---
title: Certificados
---

# Certificados de participação

Participantes que necessitam de um certificado de participação devem adotar o
seguinte procedimento:

* Fazer o download do modelo de certificado (em breve).
* Imprimir, preencher o seu nome e trazer o certificado impresso.
* Solicitar assinatura durante o evento.
* Os certificados devem ser entregues na secretaria do evento até o final de
  sexta-feira (13/04). Os certificados assinados serão entregues no sábado
  (14/04) durante o intervalo da tarde.

Certificados para voluntários

Voluntários na organização que necessitam de um certificado devem adotar o
seguinte procedimento:

* Fazer o download do modelo de certificado (em breve).
* Imprimir, preencher o seu nome e trazer o certificado impresso. Não preencher
  o campo da carga horária.
* Solicitar assinatura durante o evento.
* Os certificados devem ser entregues na secretaria do evento até às 13h do
  sábado (14/04) para ser assinado. Os certificados assinados serão devolvidos
  no sábado até o final do evento.
* Se você não for ao evento no sábado, é só ir até a secretaria pedir para ser
  assinado na sexta-feira.

**Não se preocupe quanto a rigidez dos procedimentos acima. Se tiver qualquer
dúvida, nos procure durante o evento que teremos o maior prazer em resolver a
questão. O importante é você sair do evento com o certificado já assinado
porque não iremos emitir certificados após o evento.**
