---
title: Home
---

# MiniDebConf Curitiba 2018

Welcome to the MiniDebConf Curitiba 2018 website. The conference will take
place from April 11th to 14th, 2018, at the central Campus of the Federal
Technological University or Paraná (UTFPR). [Learn more](about/).

Registration for attendance if free and [can be done here](registration/).

If you want to help us organize this event, take a look at this
[page](volunteers/).

You can [submit activity proposals](call-for-proposals/) until January 31st,
2018.

## Sponsors

[Become a sponsor](become-sponsor/)!

## Organization

  [link-debianbrasil]: http://debianbrasil.org.br
  [image-debianbrasil]: /images/logo-debianbrasil.png "Debian Brasil"

  [link-curitibalivre]: http://curitibalivre.org.br
  [image-curitibalivre]: /images/logo-curitibalivre.png "Curitiba Livre"

  [link-utfpr]: http://portal.utfpr.edu.br
  [image-utfpr]: /images/logo-utfpr.png "UTFPR"


[![Debian Brasil][image-debianbrasil]][link-debianbrasil]
[![Curitiba Livre][image-curitibalivre]][link-curitibalivre]
[![UTFPR][image-utfpr]][link-utfpr]
