---
title: Curitiba
---

# Accommodation

Near to UTFPR there are several hotels that you can walk to.

[Nacional Inn](http://www.nacionalinn.com.br/curitiba/nacional-inn-curitiba)

* Address: Rua Lourenço Pinto, 458
* Single: R$ 159,00
* Double: R$ 189,00
* Triple: R$ 239,00

[San Juan Executive](https://www.sanjuanhoteis.com.br/pt-br/hoteis/detalhes/1/san-juan-executive)

* Address: Av. Sete de Setembro, 2516
* Single: R$ 165,00
* Double: R$ 195,00
* Triple: R$ 245,00

[Victoria Villa Hotel](http://victoriavillahotel.com.br)

* Address: Av. Sete de Setembro, 2448
* Single: R$ 221,00
* Double: R$ 271,00
* Triple:R$ 321,00

[Lizon Curitiba](http://www.lizon.com.br)

* Address: Av. Sete de Setembro, 2246
* Single: R$ 159,00
* Double: R$ 189,00
* Triple: R$ 259, 00

[Nacional Inn Torres](http://www.nacionalinn.com.br/curitiba/nacional-inn-torres)

* Address: R. Mariano Torres, 976
* Single: R$ 159,00
* Double: R$ 199,00
* Triple: R$ 249,00

Ibis Budget

* Address: R. Mariano Torres, 927
* Single: R$ 145,00
* Double: R$ 145,00

Go Inn

* Address: Rua Des. Motta, 1221
* Single: R$ 198,00
* Double: R$ 206,00

# Food

Curitiba is a multicultural city, and there are several different options of
food, be it in restaurants, food trucks, food courts, etc.

The principal food in Brazil is rice, beans, meet (cow, chicken, pork, fish),
and other accompaniments like potato (fried or cooked), all kind of vegetables
like lettuce, tomato, onion, carrot, beet, etc.

It's very common a kind of restaurante called "buffet" where you pay a fixed
price or you pay per weight.
[Here some examples](https://www.google.com.br/search?tbm=isch&q=buffet+brasileiro&cad=h),
and [others examples](https://www.google.com.br/search?tbm=isch&q=buffet+brasileiro+saladas&cad=h).

But it's very common you find restaurants with chinese food, japanese food,
italian food, arabian food.

Brazilians love pizzas, so we can find many pizzerias here :-) And brazilians
love sandwichs, so you can find local manufacturers and internacional fast food
like Burger King and Mcdonald's

And sure, churrascaria (brazilian steakhouse). Where you pay a fixed price and
can eat all kind of meet.

## Vegan

Despite the fact that most brazilians eats lots of meat, Curitiba has a big and growing Vegan community, and it should not be a problem for Vegan people to be well fed in the city. There are many vegan restaurants around.

Even in restaurants where they serve meat, one will usually be able to get a decent Vegan meal at [all-you-can-eat buffets](https://en.wikipedia.org/wiki/Buffet#Restaurant_buffets) that are very popular in Brazil, mainly inside shopping malls.

* [30 Vegan / Vegetarian Friendly Restaurants by Happy Cow](https://www.happycow.net/south_america/brazil/curitiba)
* [15 by VegGuide](https://www.vegguide.org/region/844)
* [Semente de Girassol - a cheap and pretty good vegan/vegetarian restaurant](https://www.tripadvisor.com.br/Restaurant_Review-g303441-d5338849-Reviews-Semente_de_Girassol-Curitiba_State_of_Parana.html)

# What to do in Curitiba

## Tourist information

* [Bus Tourism Line](http://www.curitiba.pr.gov.br/idioma/ingles/linhaturismo) (in English)
* [Curitiba Turismo](http://www.turismo.curitiba.pr.gov.br) (in Portuguese)
* [Wikivoyage about Curitiba](https://en.wikivoyage.org/wiki/Curitiba) (in English)

## Places to visit

[CNN has selected 3 sights from here among the 20 most beautiful in Brazil!](http://edition.cnn.com/2014/06/09/travel/gallery/beautiful-brazil/index.html)

* [Municipal Market](http://www.turismo.curitiba.pr.gov.br/fotos/mercado-municipal/12/5/)
* [Opera de Arame](http://www.turismo.curitiba.pr.gov.br/fotos/opera-de-arame/157/101/)
* Panoramic Tower (Torre Panorâmica)

### Parks / Gardens

* [Bosque do Alemão](http://www.turismo.curitiba.pr.gov.br/fotos/bosque-alemao/152/77/)
* Bosque de Portugual
* Bosque do Papa
* [Botanical Garden (Jardim Botânico)](ttp://www.turismo.curitiba.pr.gov.br/fotos/jardim-botanico/9/100/)
* Barigui Park (Parque Barigui)
* Parque dos Tropeiros
* Parque Bacacheri
* [Memorial Ucraniano](http://www.turismo.curitiba.pr.gov.br/fotos/memorial-ucraniano/113/60/)
* [Parque São Lourenço](http://www.turismo.curitiba.pr.gov.br/fotos/parque-sao-lourenco/119/62/)
* [Parque Tanguá](http://www.turismo.curitiba.pr.gov.br/fotos/parque-tangua/116/61/)
* [Japan Square](http://www.turismo.curitiba.pr.gov.br/fotos/praca-do-japao/109/58/)

### Museums (Museus)

* [Alfredo Andersen](http://www.maa.pr.gov.br/)
* [Arte Sacra](http://www.fundacaoculturaldecuritiba.com.br/espacos-culturais/museu-de-arte-sacra-r-masac/)
* [Automóvel](http://www.museuautomovel.com.br/)
* Botânico Municipal
* Casa Andrade Muricy
* [Do Holocausto De Curitiba](http://www.museudoholocausto.org.br/en/)
* Egípcio - Ordem Rosa Cruz
* [Museu de Arte Contemporânea](http://www.mac.pr.gov.br/)
* [Oscar Niemeyer - MON](http://www.museuoscarniemeyer.org.br/home)
* Museu Ferroviário
* Ucraniano

### Historical sector

* [Palace of Liberty (Paço da Liberdade)](http://www.turismo.curitiba.pr.gov.br/fotos/paco-da-liberdade/15/6)
* Order of Lago (Lago da Ordem)










