---
title: Codes of Conduct
---

# Codes of Conduct and Anti-Harassment Policy

MiniDebConf Curitiba is committed to a safe environment for all participants.
All attendees are expected to treat all people and facilities with respect and
help create a welcoming environment. If you notice behaviour that fails to meet
this standard, please speak up and help to keep MiniDebConf Curitiba as
respectful as we expect it to be.

If you are harassed and requests to stop are not successful, or notice a
disrespectful environment, the organizers want to help. Please contact us at
<antiassedio@curitibalivre.org.br>. We will treat your request with dignity and
confidentiality, investigate, and take whatever actions appropriate. We can
provide information on security, emergency services, transportation,
alternative accommodations, or whatever else may be necessary. If mediation is
not successful, MiniDebConf Curitiba reserves the right to to take action
against those who do not cease unacceptable behaviour.

See the MiniDebConf Curitiba Code of Conduct below and the
[Debian Code of Conduct](https://www.debian.org/code_of_conduct).

## MiniDebConf Curitiba Code of Conduct

MiniDebConf Curitiba, as part of the greater Debian Community, assumes good faith on
all those who wish to improve Debian. However, other experiences at other
conferences have shown us the need to adopt a Code of Conduct in which we state
our expectations of all attendees and organizers during the Debian Conference.

This code of conduct applies to all attendees at MiniDebConf Curitiba, in addition
to the [Debian code of conduct](https://www.debian.org/code_of_conduct) that
applies to the Debian community as a whole.

### Debian Diversity Statement

The Debian Project welcomes and encourages participation by everyone.

No matter how you identify yourself or how others perceive you: we
welcome you. We welcome contributions from everyone as long as they
interact constructively with our community.

While much of the work for our project is technical in nature, we value
and encourage contributions from those with expertise in other areas,
and welcome them into our community.

### Be excellent to each other

MiniDebConf Curitiba is committed to providing a safe environment for all
participants. All attendees are expected to treat all people and
facilities with respect and help create a welcoming environment. If you
notice behavior that fails to meet this standard, please speak up and
help to keep MiniDebConf Curitiba as respectful as we expect it to be.

MiniDebConf Curitiba is committed to the ideals expressed in our Diversity
Statement (above) and the recently adopted Debian Code of Conduct. We ask all
our members, speakers, volunteers, attendees and guests to adopt these
principles. We are a diverse community. Sometimes this means we need to work
harder to ensure we're creating an environment of trust and respect where all
who come to participate feel comfortable and included.

We value your participation and appreciate your help in realizing this
goal.

### Be respectful

Respect yourself, and respect others. Be courteous to those around you.
If someone indicates they don't wish to be photographed, respect that
wish. If someone indicates they would like to be left alone, let them
be. Our event venues and online spaces may be shared with members of the
public; please be considerate to all patrons of these locations, even if
they are not involved in the conference.

### Be inclusive

By default, all presentation material should be suitable for people aged
12 and above.

If you could reasonably assume that some people may be offended by your
talk, please state so explicitly in the submission notes. This will be
taken into account by the Content Team. In case you are unsure if this
applies to you, please contact the Content Team at
<brasil.mini@debconf.org>. Please note that you are solely responsible if
anything is deemed inappropriate and you did not contact the Content
Team beforehand.

### Be aware

We ask everyone to be aware that we will not tolerate intimidation,
harassment, or any abusive, discriminatory or derogatory behavior by
anyone at any Debian event or in related online media.

Complaints can be made to the organizers by contacting the registration

desk or emailing <antiassedio@curitibalivre.org.br>. All complaints made to
event organizers will remain confidential and be taken seriously. The
complaint will be treated appropriately and with discretion. Should
event organizers or moderators consider it appropriate, measures they
may take can include:

- the individuals may be told to apologize
- the individuals may be told to stop/modify their behavior
  appropriately
- the individuals may be warned that enforcement action may be taken
  if the behavior continues
- the individuals may be asked to immediately leave the venue and/or
  will be prohibited from continuing to attend any part of the event
- the incident may be reported to the appropriate authorities

### What does that mean for me?

All participants, including event attendees and speakers must not engage
in any intimidation, harassment, or abusive or discriminatory behavior.

The following is a list of examples of behavior that is deemed highly
inappropriate and will not be tolerated at MiniDebConf Curitiba:

- offensive verbal or written remarks related to gender, sexual
  orientation, disability, physical appearance, body size, race, or
  religion;
- sexual or violent images in public spaces (including presentation
  slides);
- deliberate intimidation;
- stalking or following;
- unwanted photography or recording;
- sustained disruption of talks or other events;
- unwelcome physical contact or other forms of assault;
- unwelcome sexual attention;
- sexist, racist, or other exclusionary jokes;
- unwarranted exclusion from conference or related events based on
  age, gender, sexual orientation, disability, physical appearance,
  body size, race, religion;

We want everyone to have a good time at our events.

### Questions?

If you’re not sure about anything in this conference Code of Conduct,
please contact the MiniDebConf organizers at
<debian-br-eventos@lists.alioth.debian.org>.

If you wish to report a violation of this Code of Conduct, please contact
<antiassedio@curitibalivre.org.br>

### Our Promise to You.

- We will read every complaint and have several people on that alias
  that can help investigate and resolve the complaint.
- We will reply, in writing, as soon as possible to acknowledge the
  concern and assure that the matter is being investigated.
- Depending on the situation, we will talk to the reporter, the
  reported, or both to determine what mediation and/or action is
  necessary.
- Depending on the outcome of the investigation and mediation, we
  reserve the right to expel people not in compliance with our Code of
  Conduct from the venue. Debian, the MiniDebConf Organizing Committee and
  the venue in which the MiniDebConf is being held will not be held
  responsible for further costs incurred by the dismissal from the
  conference.
