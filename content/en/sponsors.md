---
title: Sponsors
---

# Sponsors

  [link-collabora]: https://www.collabora.com
  [image-collabora]: /images/logo-collabora.png "Collabora"

[![Collabora][image-collabora]][link-collabora]

# Suporters

  [link-apufpr]: http://apufpr.org.br
  [image-apufpr]: /images/logo-apufpr.png "Apufpr"

[![Apufpr][image-apufpr]][link-apufpr]