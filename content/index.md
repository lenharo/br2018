---
title: Início
---

# MiniDebConf Curitiba 2018

Bem-vindo(a) ao site da MiniDebConf Curitiba 2018 que acontecerá de 11 a 14 de
abril no Campus Central da UTFPR - Universidade Tecnológica Federal do Paraná.
Veja [mais informações](about).

Você pode realizar a sua inscrição gratuitamente [aqui](registration/).

## Chamada de voluntários

Caso queira nos ajudar na organização do evento, dê uma olhada na nossa
[chamada de voluntários](volunteers).

## Hospedagem solidária

Precisa de um local para se hospedar, ou quer oferecer hospedagem para alguém? 
Veja nossa página de [hospedagem solidária](couchsurfing).

<!--
Até o dia 31 de janeiro de 2018 você pode enviar uma proposta de atividade na
página da nosssa [chamada de atividades](call-for-proposals).
-->

## Patrocínio Ouro

  [link-collabora]: https://www.collabora.com.br
  [image-collabora]: /images/logo-collabora.png "Collabora"

[![Collabora][image-collabora]][link-collabora]

## Patrocínio Prata

  [link-4linux]: http://www.4linux.com.br
  [image-4linux]: /images/logo-4linux.png "4linux"

[![4linux][image-4linux]][link-4linux]

<!--
## Apoio

  [link-apufpr]: http://apufpr.org.br
  [image-apufpr]: /images/logo-apufpr.png "Apufpr"

[![Apufpr][image-apufpr]][link-apufpr]
-->

## Organização

  [link-debianbrasil]: http://debianbrasil.org.br
  [image-debianbrasil]: /images/logo-debianbrasil.png "Debian Brasil"

  [link-curitibalivre]: http://curitibalivre.org.br
  [image-curitibalivre]: /images/logo-curitibalivre.png "Curitiba Livre"

  [link-utfpr]: http://portal.utfpr.edu.br
  [image-utfpr]: /images/logo-utfpr.png "UTFPR"


[![Debian Brasil][image-debianbrasil]][link-debianbrasil]
[![Curitiba Livre][image-curitibalivre]][link-curitibalivre]
[![UTFPR][image-utfpr]][link-utfpr]
